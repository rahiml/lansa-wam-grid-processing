import LANSAGrid from "./grid.js";
import 'array.prototype.findindex';
import 'array.prototype.fill';

// bundle everything together
// make it part of the window object for browser use
window.LANSAGrid = LANSAGrid;