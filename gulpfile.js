var gulp = require('gulp');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var babelify = require('babelify');
var watchify = require('watchify');

function compile(watch) {
  var bundler = watchify(browserify({entries: './src/main.js', extensions: ['.js'], debug: true}).transform(babelify, {presets: ['es2015']}));

  function rebundle() {
    bundler.bundle()
      .on('error', function(err) {
        console.error(err);
        this.emit('end');
      })
      .pipe(source('LANSA-grid.min.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(uglify())
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./dist/'));
  }

  if (watch) {
    bundler.on('update', function() {
      console.log('-> bundling...');
      rebundle();
    });
  }

  rebundle();
}

function watch() {
  return compile(true);
};

gulp.task('build', function() {
  return compile();
});
gulp.task('watch', function() {
  return watch();
});

gulp.task('default', ['watch']);