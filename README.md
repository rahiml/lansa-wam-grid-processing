# README #

This is a set of client side javascript utilities to work with LANSA generated grid/list markup.

### What is this repository for? ###

* Client side processing for LANSA grids/lists for features such as: summing up rows, dynamically add new rows
* Keyboard arrow and enter keys enabled for spreadsheet-like processing
* Code was written using markup from LANSA V13 SP2, but should work on other versions if markup is the same

### How do I get set up? ###

1. Make sure your application already has jQuery included or use a CDN:

        <script src="https://code.jquery.com/jquery.min.js"></script>

2. If IE8 support is needed, add conditional shims:

        <!--[if lt IE 9]>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/4.3.1/es5-shim.min.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/es5-shim/4.3.1/es5-sham.min.js"></script>
        <![endif]-->

3. Download the main grid distribution located in ./dist/LANSA-grid.min.js of this repo to your webserver.

4. Include main grid distribution in your web app:

        <script src="{YOUR WEBSERVER PATH}/LANSA-grid.min.js"></script>

### Usage ###

* **Define** - Any CSS selector can be used (jQuery is used under the covers). The selector should be for the main table of the LANSA grid.
        
        var grid = new LANSAGrid('#XMTLENTRY');

* **Subscribe** - The subscribe method can be used to register a callback for column changes. Columns are numbered from the left most in the grid starting from 1. Hidden columns are also included.  The subscribe method expects a column and a callback function.  The callback function will be called with the result of the column change, currently defaulted to the sum of the column.  The callback function will be called with the resulting value of the column (sum).

        grid.subscribe({
          column: 4,
          callback: function (sumOfCards) {
            $('.totalCardsOutput').html(sumOfCards.toFixed(0) || "0");
          }
        });

* **Define Calculation** - The defineCalculation method can be used to define a calculation formula for a column.  This is for dynamic columns that need to be updated when the values of other columns are changed by the user.  Only user input columns can be used for calculations, not other dynamic columns.  The defineCalculation method expects a column, a definition function, a format string and optionally a render function.  The definition function takes a column object as input, and returns the value of the calculated column using values from other columns.  The format string defines the format of the result of the calculation for output in the grid.  Optionally, a render function can be defined, returning a template string, where {{value}} represents the value of the calculation.

        grid.defineCalculation({
            column: 7,
            // columns object represents the values of columns in grid
            // use this to define the calculation for a the column
            definition: function (columns) {
                return (columns[4] * (nationalDues + departmentDues)) - columns[5];
            },
            // optionally can define a render function for each cell of the column
            // by default will just output the formatted value of the cell inside the <td>
            // uses doT.js as the template engine: http://olado.github.io/doT
            // {{value}} will be replaced by the formatted value of the cell
            /*
            render: function () {
                return '<input value="{{value}}" />';
            },
            */
            format: '$0,0.00'   // use any format from numeraljs: http://numeraljs.com/
        });

* **Add Row** - The addRow method can add one or multiple rows to the LANSA grid. It will take care of keeping the indexes up to date so that the list will be submitted back to LANSA properly.

        $('.addRow').on('click', function () {
          grid.addRow(1);
        });

* **Get Column Sum** - The getColumnSum method returns the sum of a column.  It takes a single parameter (column index starting from 1).

* **Update Calculations** - The updateCalculations method allows you to programatically update defined grid calculations.  This is useful for running grid calculations with default values that may come from the server.

### Examples ###
* Download the repository and run index.html in your browser to see a working example.

### Contact ###

* For any questions or inquiries, please contact Marco Kam - [marco.kam@lansa.com](mailto:marco.kam@lansa.com)